from importlib import import_module
import re

from commands import _util
from general import game_log
import trpg


def dispatch(command, message):
    """handle command
    :param command:
    :return: output content to discord
    """
    game = trpg.Game(message.channel)

    if command == 'init':
        if not game.game.sections():
            game.init(message.author)
            return "建立遊戲"
        else:
            return "本頻道已經有遊戲"

    if not game.game.sections():
        return "本頻道並未建立遊戲"

    if game.is_gm(message.author):
        players = re.findall('<@?(.+?)>', command)
        command = re.sub('<@?(.+?)>', '', command)
    else:
        players = [message.author]

    commands = command.split(' ')
    commands = [element for element in commands if element != '']

    command = None
    arguments = []

    for item in commands:
        if not command:
            command = item
        else:
            arguments.append(item)

    # execute global commands
    try:
        general_module = import_module("general.{}".format(command))
        command_results = general_module.main(str(message.channel), game.is_gm(message.author), arguments)
        return "\n".join(command_results)
    except ImportError:
        pass

    if game.is_gm(message.author) and len(players) == 0:
        players = game.get_players()

    print("players: {}".format(players))
    print("command: {}".format(command))
    print("arguments: {}".format(arguments))

    if command == 'join':
        if len(players) != 1:
            raise Exception("GM無法同時擔任玩家")
        if len(arguments) != 1:
            raise Exception("Usage: join {character name}")
        game.create_player(players[0], arguments[0])
        return "{} joined as {}".format(players[0], arguments[0])

    results = []
    for player in players:
        pc_name = game.get_pc_info(player)['name']
                
        try:
            command_module = import_module("commands.{}".format(command))
        except ImportError:
            return "Invalid command: {}".format(command)

        command_results = command_module.main(game, player, arguments)
        for result in command_results:
            result = output_format_dict(result) if type(result) == dict else result
            results.append("{}: {}".format(pc_name, result))

    game_log.write_dice_result(game.game_name, "\n".join(results))
    return "\n".join(results)


def get_character_names(game, discord_account, character_name=None):
    if game.is_player(discord_account):
        return game.get_player_character_names(discord_account)
    if game.is_gm(discord_account):
        if character_name == '*':
            return game.get_player_character_names(discord_account)
        else:
            return [character_name]


def output_format_dict(skill_dict):
    results = []
    for key in skill_dict:
        results.append("{}({})".format(key, skill_dict[key]))
    return " ".join(results)
