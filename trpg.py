import configparser
import random
import json
import shutil


class Game:
    def __init__(self, game_name):
        self.game = configparser.ConfigParser()
        self.game_name = game_name
        self.game.read('games/{}'.format(self.game_name))

    def init(self, discord_account):
        template_file = "games/__template__"
        game_file = "games/{}".format(self.game_name)
        shutil.copyfile(template_file, game_file)
        self.game.read(game_file)
        self.game.set('__game__', 'gm', str(discord_account))
        self.__save_game__()

    def is_gm(self, discord_account):
        if str(discord_account) == self.game.get('__game__', 'gm'):
            return True
        return False

    def is_player(self, discord_account):
        players = json.loads(self.game.get('__game__', 'pl'))
        if str(discord_account) in players:
            return True
        return False

    def create_player(self, discord_account, pc_name):
        players = json.loads(self.game.get('__game__', 'pl'))
        players.append(str(discord_account))
        self.game.set('__game__', 'pl', json.dumps(players))
        self.game.add_section(str(discord_account))
        self.game.set(str(discord_account), 'name', pc_name)
        stat_dict = {"hp": 0, "mp": 0, "san": 0, "luk": 0}
        attr_dict = {"str": 50, "con": 50, "dex": 50, "app": 50, "pow": 50, "siz": 50, "int": 50, "edu": 50}
        self.game.set(str(discord_account), 'stat', json.dumps(stat_dict))
        self.game.set(str(discord_account), 'attr', json.dumps(attr_dict))
        self.game.set(str(discord_account), 'skill', "{}")
        self.game.set(str(discord_account), 'buff', "{}")
        self.game.set(str(discord_account), 'item', "{}")
        self.__save_game__()

    def generate_player(self, discord_account, category):
        if 'attr' in category:
            attr_dict = {}
            attr_dict['str'] = roll(3, 6) * 5
            attr_dict['con'] = roll(3, 6) * 5
            attr_dict['pow'] = roll(3, 6) * 5
            attr_dict['dex'] = roll(3, 6) * 5
            attr_dict['app'] = roll(3, 6) * 5
            attr_dict['int'] = (roll(2, 6) + 6) * 5
            attr_dict['edu'] = (roll(2, 6) + 6) * 5
            attr_dict['siz'] = (roll(2, 6) + 6) * 5
            self.game.set(str(discord_account), 'attr', json.dumps(attr_dict))
            self.__save_game__()
        if 'stat' in category:
            attr_dict = self.get_pc_info(discord_account)['attr']
            stat_dict = {}
            stat_dict['hp'] = int((attr_dict['con'] + attr_dict['siz']) / 10)
            stat_dict['mp'] = int(attr_dict['pow'] / 5)
            stat_dict['san'] = attr_dict['pow']
            stat_dict['luk'] = roll(3, 6) * 5
            self.game.set(str(discord_account), 'stat', json.dumps(stat_dict))
            self.__save_game__()
        if 'skill' in category:
            attr_dict = self.get_pc_info(discord_account)['attr']
            skill_points = attr_dict['edu'] * 4 + attr_dict['int'] * 2
            skill_count = 10
            default_skill_dict = self.get_default_skill()
            skills = [skill for skill in default_skill_dict]
            skills.remove('克蘇魯神話')
            skills.remove('信用評級')
            skill_dict = {}
            for index in range(0, skill_count):
                skill_index = random.randint(0, len(skills)-1)
                skill_name = skills[skill_index]
                skill_dict[skill_name] = default_skill_dict[skill_name] + int(skill_points / skill_count)
                skills.remove(skill_name)
            self.game.set(str(discord_account), 'skill', json.dumps(skill_dict))
            self.__save_game__()

    def validate_player(self, discord_account):
        pc_info = self.get_pc_info(str(discord_account))
        stat_dict = pc_info['stat']
        stat_dict['hp'] = int((pc_info['attr']['con'] + pc_info['attr']['siz']) / 10)
        stat_dict['mp'] = int(pc_info['attr']['pow'] / 5)
        stat_dict['san'] = pc_info['attr']['pow']
        stat_dict['luk'] = roll(3, 6) * 5
        self.game.set(str(discord_account), 'stat', json.dumps(stat_dict))
        self.__save_game__()
        default_skill = self.get_default_skill()
        skill_points = pc_info['attr']['edu'] * 4 + pc_info['attr']['int'] * 2
        for skill in pc_info['skill']:
            skill_points -= pc_info['skill'][skill] - default_skill[skill]
        if skill_points != 0:
            raise Exception("skill points: {} points left".format(skill_points))

    def get_players(self):
        return json.loads(self.game.get('__game__', 'pl'))

    def get_pc_info(self, discord_account):
        pc_info = {}
        for key in self.game.options(str(discord_account)):
            pc_info[key] = self.game.get(str(discord_account), key)
            try:
                # convert to json if possible
                pc_info[key] = json.loads(pc_info[key])
            except:
                pass
        return pc_info

    def get_default_skill(self):
        return json.loads(self.game.get('default', 'skill'))

    def get_pc_value(self, discord_account, keys):
        if type(keys) == str:
            keys = [keys]
        default_skill_dict = self.get_default_skill()
        stat_dict = json.loads(self.game.get(str(discord_account), 'stat'))
        attr_dict = json.loads(self.game.get(str(discord_account), 'attr'))
        skill_dict = json.loads(self.game.get(str(discord_account), 'skill'))
        character_dict = default_skill_dict
        character_dict.update(stat_dict)
        character_dict.update(attr_dict)
        character_dict.update(skill_dict)
        result = {}
        for key in keys:
            if key in character_dict:
                result[key] = character_dict[key]
        return result
    
    def set_pc_info(self, discord_account, category, content):
        content = {k: v for k, v in content.items() if v != 0}
        self.game.set(str(discord_account), category, json.dumps(content))
        self.__save_game__()

    def get_weapon(self):
        return json.loads(self.game.get('default', 'weapon'))

    def set_weapon(self, weapon_name, skill_name, damage="1d3"):
        weapon = self.get_weapon()
        weapon[weapon_name] = {"skill": skill_name, "damage": damage}
        self.game.set('default', 'weapon', json.dumps(weapon))
        self.__save_game__()

    def __save_game__(self):
        with open('games/{}'.format(self.game_name), 'w') as config_file:
            self.game.write(config_file)


def roll(dice_total, dice_cap):
    count = 0
    result = 0
    while count < dice_total:
        result += random.randint(1, int(dice_cap))
        count += 1
    return result
