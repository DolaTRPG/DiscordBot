# Discord Bot for TRPG

現在僅支援CoC 7th



# 語法說明

所有的指令都以 `@DolaTRPG` 為開頭

如果是ＧＭ的話，可以額外多標記其他人來代替該玩家進行操作
範例：

```
@DolaTRPG @other-player join 角色名稱
```

如果ＧＭ沒有標記其他人，則會對所有玩家都使用相同指令



# 開始使用

## GM開團

```
init
```

在現在的頻道上面建立遊戲
執行開團指令的人會自動成為ＧＭ



## PL加入

```
join 角色名稱
```

加入遊戲，並建立一張空白角色卡



# 角色

## 創造角色

```
generate attr
generate stat
generate skill
```

隨機產生角色數值

`attr` = 八大屬性 (STR, CON, POW, DEX, APP, INT, EDU, SIZ)

`stat` = 其他屬性 (HP, MP, SAN, LUK)

`skill` = 隨機選10個技能，把技能點數平均分配上去



## 修改參數

```
set 參數名稱 目標數值
set hp 5
set dex -1
set 說服 +1
```

修改角色能力

如果有給 +- 符號，則會依照當前數值進行增減
否則將直接設定為目標值



## 檢查數值

```
validate
```

檢查角色數值

自動計算出對應的 HP, MP, SAN

以 EDU\*4 + INT\*2 來計算技能等級是否符合



## 角色資訊

```
info
stat
stat hp
attr
attr dex
skill
skill 偵查
item
item 道具
item 道具 5
item 道具 +1
item 道具 -1
```

`info` = 列出全部角色數值

`stat` = 列出角色附屬屬性，沒有註明項目的話則全部列出

`attr` = 列出角色八大屬性，沒有註明項目的話則全部列出

`skill` = 列出角色技能，沒有註明項目的話則全部列出

`item` = 列出道具項目，後面可以設定道具數量


# 跑團

## 擲骰檢定

``` 
roll 技能名稱 修正值
roll 電腦使用
roll 隱密行動 +20
roll 電器維修 -20
```

## 對抗

```
vs {技能名稱} {對手數值}
vs dex 70
```

## 戰鬥

使用戰鬥功能之前，必須先設定道具對應的技能以及傷害

```
attack set {道具名稱} {對照技能} {武器傷害}
attack set 左輪手槍 手槍 1d6
```


```
attack {道具名稱} {命中修正}
attack
attack -20
attack 手槍
attack 手槍 +30
```

不指定道具名稱時，會以空手做為判定。

流程：

- 檢查玩家是否持有道具
- 道具耐久度-1
- 判定命中
- 判定傷害
