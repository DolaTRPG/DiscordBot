import configparser
import requests


def user_input(game, message):
    if message.startswith(">>>>"):
        log_url = export_game_log(game)
        return log_url

    markdown_content = ""
    if message.startswith(">>>"):
        markdown_content = generate_markdown_format("title", message)
    elif message.startswith(">>"):
        markdown_content = generate_markdown_format("header", message)
    elif message.startswith(">"):
        markdown_content = generate_markdown_format("text", message)

    append_game_log(game, markdown_content)


def write_dice_result(game, dice_result):
    markdown_content = generate_markdown_format("block", dice_result)
    append_game_log(game, markdown_content)


def generate_markdown_format(type, content):
    content = content.lstrip("> ")
    if type == "title":
        return "\n# {}".format(content)
    if type == "header":
        return "\n## {}".format(content)
    if type == "text":
        return "- {}".format(content)
    if type == "block":
        return "\n```\n{}\n```\n".format(content)


def append_game_log(game_name, content):
    with open("games/{}.md".format(game_name), "a") as log:
        log.write("{}\n".format(content))


def export_game_log(game_name):
    print("save log to {}".format(game_name))

    config = configparser.ConfigParser()
    config.read('bot.conf')
    gitlab_project_id = config.get('log', 'gitlab_project_id')
    gitlab_token = config.get('log', 'gitlab_token')

    header = {"PRIVATE-TOKEN": gitlab_token}
    gitlab_url = "https://gitlab.com/api/v4/projects/{}/snippets".format(gitlab_project_id)
    with open("games/{}.md".format(game_name), "r") as log:
        request_json = {
            "title": str(game_name), "file_name": "跑團紀錄.md", "code": log.read(), "visibility": "internal"
        }
    response = requests.post(url=gitlab_url, json=request_json, headers=header)
    print(response.text)
    try:
        return response.json()['web_url']
    except Exception as e:
        return response.text
