import csv

csv_fields = ['channel', 'event', 'content']
csv_filename = 'games/_events.csv'


def main(game_name, is_gm=False, arguments=None):
    events = read_events()

    if arguments:
        if not is_gm:
            return "只有GM可以更改活動內容"

        if len(arguments) == 1:
            events = remove_events(events, game_name, arguments[0])
            write_events(events)
        elif len(arguments) == 2:
            events = update_events(events, game_name, arguments[0], arguments[1])
            write_events(events)
        else:
            return "usage: event {活動名稱} {活動內容}"

    return output_events(events, game_name)


def read_events():
    with open(csv_filename, newline='') as csvfile:
        events = csv.DictReader(csvfile)
        events = [row for row in events]
    return events


def write_events(events):
    with open(csv_filename, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=csv_fields)
        writer.writeheader()
        for event in events:
            writer.writerow(event)
    return events


def remove_events(events, game_name, event_name):
    new_events = [x for x in events if not (x['channel'] == game_name and x['event'] == event_name)]
    return new_events


def update_events(events, game_name, event_name, content):
    for event in events:
        if event['channel'] == game_name and event['event'] == event_name:
            event['content'] = content
            return
    else:
        events.append({
            'channel': game_name,
            'event': event_name,
            'content': content
        })
    return events


def output_events(events, game_name):
    results = []
    events.sort(key=lambda x: x['event'])
    for event in events:
        if event['channel'] == game_name:
            results.append("{}: {}".format(event['event'], event['content']))
    return results
