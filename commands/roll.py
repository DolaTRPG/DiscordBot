import random

from . import _util


def main(game, player, arguments):
    results = []
    if len(arguments) == 0 or len(arguments) > 2:
        return ["Usage: roll {skill name} {modifier}"]
    skill_name = arguments[0]
    modifier = 0

    buff_dict = game.get_pc_info(player)['buff']
    if buff_dict:
        buff_modifier = sum(buff_dict.values())
        modifier = buff_modifier
        buff_message = "buff修正: {} = {}".format(buff_modifier, _util.output_format_dict(buff_dict))
        results.append(buff_message)

    pc_skill = game.get_pc_value(player, skill_name)
    skill_dc = pc_skill[skill_name]
    if len(arguments) == 2:
        input_modifier = arguments[1]
        if input_modifier.startswith('/'):
            skill_dc /= int(input_modifier[1:])
        else:
            modifier += int(input_modifier)
    (result_success, result_message) = roll_skill(skill_name, int(skill_dc), modifier)
    results.append(result_message)
    return results


def roll_skill(skill_name, skill_dc, modifier=0):
    dice_result = _util.roll(1, 100)
    if dice_result in range(1, 3):
        result_text = "大成功"
        success = True
    elif dice_result in range(99, 101):
        result_text = "大失敗"
        success = False
    elif dice_result <= (skill_dc + int(modifier)):
        result_text = "成功"
        success = True
    else:
        result_text = "失敗"
        success = False
    return (success, "{}({}+{}): {}({})".format(skill_name, skill_dc, int(modifier), dice_result, result_text))
