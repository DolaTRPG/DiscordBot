def main(game, player, arguments):
    results = []
    for index in ['stat', 'attr', 'skill', 'item', 'buff']:
        pc_dict = game.get_pc_info(player)[index]
        results.append(pc_dict)
    return results
