def main(game, player, arguments):
    results = []
    game.validate_player(player)
    for index in ['stat', 'attr', 'skill']:
        pc_dict = game.get_pc_info(player)[index]
        results.append(pc_dict)
    return results
