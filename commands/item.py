def main(game, player, arguments):
    if len(arguments) > 2:
        return "Usage: item {item name} {count}"
    pc_item = game.get_pc_info(player)['item']
    if len(arguments) == 1:
        item_name = arguments[0]
        if item_name in pc_item:
            pc_item = {item_name: pc_item[item_name]}
        else:
            return ("並未持有道具：{}".format(item_name))
    if len(arguments) == 2:
        item_name = arguments[0]
        item_count = arguments[1]
        if item_count.startswith('+') or item_count.startswith('-'):
            pc_item[item_name] += int(item_count)
        else:
            pc_item[item_name] = int(item_count)
        game.set_pc_info(player, 'item', pc_item)
    return [pc_item]
