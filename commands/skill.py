from . import _util


def main(game, player, arguments):
    """
    skill : list all skills for player
    skill {skill_name} : list specified skill for player
    skill {skill_name} {new_value} : set value for skill
    skill {skill_name} {modifier} : set value based on original value for skill
    """
    pc_skill = game.get_pc_info(player)['skill']
    if len(arguments) == 0:
        return [pc_skill]

    skill_name = arguments[0]

    if len(arguments) == 1:
        default_skill_dict = game.get_default_skill()
        default_skill_dict.update(pc_skill)
        result_skill = {}
        if skill_name not in default_skill_dict:
            return ["沒有{}技能".format(skill_name)]
        result_skill[skill_name] = default_skill_dict[skill_name]
        return [result_skill]

    if len(arguments) == 2:
        old_value = int(get_skill_value(game, pc_skill, skill_name))
        new_value = _util.dice_string_to_value(arguments[1])

        if new_value.startswith(('+', '-')):
            new_value = old_value + int(new_value)

        pc_skill[skill_name] = int(new_value)
        game.set_pc_info(player, 'skill', pc_skill)
        return ["{}: {}->{} ({:+d})".format(skill_name, old_value, new_value, int(new_value) - old_value)]

    return ["usage: skill {skill_name} {new_value}"]


def get_skill_value(game, skill_dict, skill_name):
    # return value if in skill_dict
    if skill_name in skill_dict:
        return skill_dict[skill_name]

    # return default value
    default_skill_dict = game.get_default_skill()
    if skill_name not in default_skill_dict:
        return 0
    return default_skill_dict[skill_name]
