import random


def main(game, player, arguments):
    if len(arguments) >= 3:
        return "Usage: vs {skill} {target value}"
    skill_name = arguments[0]
    target_value = int(arguments[1])

    base_dc = 50
    pc_skill_value = game.get_pc_value(player, skill_name)[skill_name]
    dc = base_dc + (pc_skill_value - target_value)
    (vs_success, vs_message) = roll_skill("{}對抗".format(skill_name), dc)
    return [vs_message]


def roll_skill(skill_name, skill_dc, modifier=0):
    dice_result = roll(1, 100)
    if dice_result in range(1, 3):
        result_text = "大成功"
        success = True
    elif dice_result in range(99, 101):
        result_text = "大失敗"
        success = False
    elif dice_result <= (skill_dc + int(modifier)):
        result_text = "成功"
        success = True
    else:
        result_text = "失敗"
        success = False
    return (success, "{}({}+{}): {}({})".format(skill_name, skill_dc, int(modifier), dice_result, result_text))


def roll(dice_total, dice_cap):
    count = 0
    result = 0
    while count < dice_total:
        result += random.randint(1, int(dice_cap))
        count += 1
    return result
