import configparser
import random
import requests


def dice_string_to_value(dice_string):
    if 'd' not in dice_string:
        return dice_string

    if dice_string.startswith(('+', '-')):
        prefix = dice_string[0]
        dice_string = dice_string[1:]
    else:
        prefix = ""

    dice_count = dice_string.split('d')[0]
    dice_cap = dice_string.split('d')[1]
    value = roll(dice_count, dice_cap)

    return "{}{}".format(prefix, value)


def roll(dice_total, dice_cap):
    count = 0
    result = 0
    while count < int(dice_total):
        result += random.randint(1, int(dice_cap))
        count += 1
    return result


def output_format_dict(skill_dict):
    results = []
    for key in skill_dict:
        results.append("{}({})".format(key, skill_dict[key]))
    return " ".join(results)
