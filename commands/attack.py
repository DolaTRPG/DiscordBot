import random

from . import _util


def main(game, player, arguments):
    results = []
    item_name = None
    if len(arguments) >= 3:
        if arguments[0] == 'set':
            weapon_name = arguments[1]
            skill_name = arguments[2]
            damage = arguments[3]
            game.set_weapon(weapon_name, skill_name, damage)
            return ["{}: {} 傷害:{}".format(weapon_name, skill_name, damage)]
        else:
            return ["Usage: attack {item name} {modifier}"]
    if len(arguments) >= 2:
        item_name = arguments[0]
        hit_modifier = int(arguments[1])
    if len(arguments) == 1:
        if arguments[0].startswith('+') or arguments[0].startswith('-'):
            item_name = None
            hit_modifier = int(arguments[0])
        else:
            item_name = arguments[0]
            hit_modifier = 0
    if len(arguments) == 0:
        item_name = None
        hit_modifier = 0

    # set item duration
    pc_item = game.get_pc_info(player)['item']
    if item_name and item_name not in pc_item:
        return["並未持有{}".format(item_name)]
    elif item_name:
        duration_decrease = 1
        results.append(
            "{}耐久度減少({}->{})".format(item_name, pc_item[item_name], pc_item[item_name] - duration_decrease))
        pc_item[item_name] -= duration_decrease
        if pc_item[item_name] == 0:
            results.append("{}損毀".format(item_name))
            pc_item.pop(item_name)
        game.set_pc_info(player, 'item', pc_item)

    # get weapon settings
    weapon_name = item_name or "空手"
    weapon = game.get_weapon()
    if weapon_name not in weapon:
        return["GM並未設定{}的武器屬性".format(weapon_name)]
    else:
        skill_name = weapon[weapon_name]['skill']
        damage_roll = weapon[weapon_name]['damage']

    # roll hit
    buff_dict = game.get_pc_info(player)['buff']
    if buff_dict:
        buff_modifier = sum(buff_dict.values())
        hit_modifier += buff_modifier
        buff_message = "buff修正: {} = {}".format(buff_modifier, _util.output_format_dict(buff_dict))
        results.append(buff_message)

    pc_skill = game.get_pc_value(player, skill_name)
    (hit_success, hit_message) = roll_skill(skill_name, pc_skill[skill_name], hit_modifier)
    results.append("{}".format(hit_message))

    # roll damage
    if hit_success:
        damage = roll(int(damage_roll.split('d')[0]), int(damage_roll.split('d')[1]))
        results.append("武器傷害 {} = {}".format(damage_roll, damage))
        if skill_name in ["格鬥", "近戰", "遠程", "投擲"]:
            pc_attr = game.get_pc_info(player)['attr']
            damage_bonus = 0
            if pc_attr['str'] + pc_attr['siz'] < 65:
                damage_bonus = "-2"
            elif pc_attr['str'] + pc_attr['siz'] < 85:
                damage_bonus = "-1"
            elif pc_attr['str'] + pc_attr['siz'] < 125:
                damage_bonus = "0"
            elif pc_attr['str'] + pc_attr['siz'] < 165:
                damage_bonus = "1d4"
            elif pc_attr['str'] + pc_attr['siz'] < 284:
                damage_bonus = "1d6"
            damage_bonus_result = _util.dice_string_to_value(damage_bonus)
            results.append("體型傷害 {} = {}".format(damage_bonus, damage_bonus_result))
            results.append("合計傷害 = {}".format(int(damage)+int(damage_bonus_result)))
    return results


def roll_skill(skill_name, skill_dc, modifier=0):
    dice_result = roll(1, 100)
    if dice_result in range(1, 3):
        result_text = "大成功"
        success = True
    elif dice_result in range(99, 101):
        result_text = "大失敗"
        success = False
    elif dice_result <= (skill_dc + int(modifier)):
        result_text = "成功"
        success = True
    else:
        result_text = "失敗"
        success = False
    return (success, "{}({}+{}): {}({})".format(skill_name, skill_dc, int(modifier), dice_result, result_text))


def roll(dice_total, dice_cap):
    count = 0
    result = 0
    while count < dice_total:
        result += random.randint(1, int(dice_cap))
        count += 1
    return result
