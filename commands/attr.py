from . import _util


def main(game, player, arguments):
    """
    attr : list all attr for player
    attr {attr_name} : list specified attr for player
    attr {attr_name} {new_value} : set attr value
    attr {attr_name} {modifier} : set attr value based on original value
    """
    pc_attr = game.get_pc_info(player)['attr']
    if len(arguments) == 0:
        return [pc_attr]

    attr_name = arguments[0]

    if len(arguments) == 1:
        return [pc_attr[attr_name]]

    if len(arguments) == 2:
        old_value = int(pc_attr[attr_name])
        new_value = _util.dice_string_to_value(arguments[1])

        if str(new_value).startswith(('+', '-')):
            new_value = old_value + int(new_value)

        pc_attr[attr_name] = int(new_value)
        game.set_pc_info(player, 'attr', pc_attr)
        return ["{}: {}->{} ({:+d})".format(attr_name, old_value, new_value, int(new_value)-old_value)]

    return ["usage: attr {stat_name} {new_value}"]
