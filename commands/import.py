def main(game, player, arguments):
    """
    import {category} {sets} : batch update character data
    import attr str(30)
    """
    category = arguments[0]
    new_dict = {}
    for item in arguments[1:]:
        (key, value) = parse_key_value(item)
        new_dict[key] = int(value)
    game.set_pc_info(player, category, new_dict)
    return [new_dict]


def parse_key_value(kv_string):
    """
    separate string "key(value)" into tuple (key, value)
    """
    key = kv_string.split('(')[0]
    value = kv_string.split('(')[1].split(')')[0]
    return (key, value)
