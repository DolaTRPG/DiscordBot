from . import _util


def main(game, player, arguments):
    """
    stat : list all stat for player
    stat {stat_name} : list specified stat for player
    stat {stat_name} {new_value} : set stat value
    stat {stat_name} {modifier} : set stat value based on original value
    """
    pc_stat = game.get_pc_info(player)['stat']
    if len(arguments) == 0:
        return [pc_stat]

    stat_name = arguments[0]

    if len(arguments) == 1:
        return [pc_stat[stat_name]]

    if len(arguments) == 2:
        old_value = int(pc_stat[stat_name])
        new_value = _util.dice_string_to_value(arguments[1])

        if str(new_value).startswith(('+', '-')):
            new_value = old_value + int(new_value)

        pc_stat[stat_name] = int(new_value)
        game.set_pc_info(player, 'stat', pc_stat)
        return ["{}: {}->{} ({:+d})".format(stat_name, old_value, new_value, int(new_value)-old_value)]

    return ["usage: stat {stat_name} {new_value}"]
