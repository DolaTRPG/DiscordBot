def main(game, player, arguments):
    result = []
    game.generate_player(player, arguments)
    for index in ['stat', 'attr', 'skill', 'item']:
        pc_dict = game.get_pc_info(player)[index]
        result.append(pc_dict)
    return result
