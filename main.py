import configparser
import re
import discord

import handler
from general import game_log

config = configparser.ConfigParser()
config.read('bot.conf')

TOKEN = config.get('bot', 'token')
USER = {}

client = discord.Client()


async def replace_mention_with_username(content):
    mention_list = re.findall('<@\!?\d*>', content)
    print(mention_list)
    result = content
    for mention_tag in mention_list:
        user_id = re.findall('\d+', mention_tag)[0]
        user_name = USER[user_id]
        result = result.replace(mention_tag, "<{}>".format(user_name))
    return result


@client.event
async def on_message(message):
    if message.content.startswith(">"):
        response = game_log.user_input(message.channel, message.content)
        if response:
            await client.send_message(message.channel, response)
        return

    # command needs to start with tag to bots
    command_prefix = '<@{}>'.format(client.user.id)
    if not message.content.startswith(command_prefix):
        return
    print("{}: {}: {}".format(message.channel, message.author, message.content))

    # we do not want the bot to reply to itself
    if message.author == client.user:
        return

    command = message.content.lstrip(command_prefix).lstrip(' ')
    command = await replace_mention_with_username(command)

    try:
        response_message = handler.dispatch(command, message)
    except Exception as e:
        response_message = "ERROR: {}".format(e)
        raise
    finally:
        response = "{}\n{}".format(message.author.mention, response_message)
        await client.send_message(message.channel, response)


def cache_user_id():
    print('loading user list')
    members = client.get_all_members()
    for member in members:
        USER[member.id] = "{}#{}".format(member.name, member.discriminator)
    print('{} users loaded'.format(len(USER)))


@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    cache_user_id()
    print('------')

client.run(TOKEN)
